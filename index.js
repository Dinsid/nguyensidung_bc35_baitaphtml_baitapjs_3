// start bai1


function luong_nv() {
    var date = document.getElementById("date").value;
    var luong_1_ngay = document.getElementById("tien1ngay").value;  
    var tong_luong = luong_1_ngay * Number(date);
    document.getElementById("Luong").value = tong_luong;
}
// end bai1
// start bai2
function sumAverage() {
    var number1 = document.getElementById("number1").value;
    var number2 = document.getElementById("number2").value;
    var number3 = document.getElementById("number3").value;
    var number4 = document.getElementById("number4").value;
    var number5 = document.getElementById("number5").value;
    var average = (Number(number1) + Number(number2) + Number(number3) + Number(number4) + Number(number5)) / 5;
    document.getElementById("KetQua").value = average;

}
// end bai2
// start bai3
const USD_VND = 23500; 
function doiTien() {    
    var tien_USD = document.getElementById("money_change").value;
    var tien_VND = Number(tien_USD) * USD_VND;
    document.getElementById("change_success").value = tien_VND;
}
// end bai3
// start bai4
function tinh() {
    var chieuDai = document.getElementById("chieuDai").value;
    var chieuRong = document.getElementById("chieuRong").value;
    var dienTichHCN = Number(chieuDai) * Number(chieuRong);
    var chuViHCN = (Number(chieuDai) + Number(chieuRong))*2;
    var contentResult = `<p> Diện tích: ${dienTichHCN}</p>
                         <p> Chu vi: ${chuViHCN} </p>   `;
    document.getElementById("result").innerHTML = contentResult;
}
// end bai4
// start bai5
function tinhTong() {
    var so = document.getElementById("number").value;
    var chuc = Math.floor(so / 10);
    var donVi = so % 10;
    var tong = Number(chuc) + Number(donVi);
    document.getElementById("sum").value = tong;
}
// end bai5